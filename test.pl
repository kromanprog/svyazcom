#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use locale;
use utf8;

sub modify_datetime;
sub modify_date;
sub modify_time;

binmode(STDOUT,":utf8");
my $inputfilepath = "D:\\tmp\\data.csv";
my $outfilepath = "D:\\tmp\\data_mod.csv";
open(my $file,"<:encoding(cp1251)",$inputfilepath) or die("Can't open file $inputfilepath");
open(my $outfile,">:encoding(utf8)",$outfilepath) or die("Can't open file $outfilepath");


my $line;
my @data;

while(<$file>)
{
    $line = $_;
    @data = split("\t",$line);
    if (($data[1] ne "") && ($data[1] ne 'NULL') && ($data[1] ne 'VISA') && ($data[1] ne 'MASTERCARD') && ($data[1] ne 'MAESTRO') && ($data[1] ne 'VISA ELECTRON'))
    {
        $_ = $data[0];
        s/,/./;

        my $datetime = modify_datetime($data[6]);
        my $date = modify_dateonly($data[6]);

        my $block1_5  = join(',',@data[1..5]);
        my $block9_11 = join(',',@data[9..11]);
        my $block13_16 = join(',',@data[13..16]);
        print '$'.$_.",".$block1_5.','.$datetime.','.$datetime.','.$date.','.$block9_11.','.$date.','.$block13_16;
        print $outfile '$'.$_.",".$block1_5.','.$datetime.','.$datetime.','.$date.','.$block9_11.','.$date.','.$block13_16;
    }
}


close($file) or die("File $inputfilepath closing error.");
close($outfile) or die("File $outfilepath closing error.");




sub modify_datetime {
    my $datetime = $_[0];
    my @datetime = split ' ',$datetime;
    return modify_date($datetime[0]).' '.modify_time($datetime[1]);
}

sub modify_dateonly {
    my $datetime = $_[0];
    my @datetime = split ' ',$datetime;
    return modify_date($datetime[0]);
}

sub modify_date {
    my @sub = split '-',$_[0];
    return '2/'.$sub[2].'/2018';
}

sub modify_time {
    return substr $_[0],0,5;
}